import 'package:app/modules/main_layout/don_taskes/don_taskes.dart';
import 'package:app/modules/main_layout/rachi_taskes/rachi_taskes.dart';
import 'package:app/modules/main_layout/taskes/new_taskes.dart';
import 'package:app/shared/cubit/states.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sqflite/sqflite.dart';

class AppCubit extends Cubit<AppStates> {
  AppCubit() : super(AppInitialState());
  static AppCubit get(context) => BlocProvider.of(context);
  int currentIndex = 0;
  List<Widget> screen = [
    NewTaskes(),
    DonTaskes(),
    RechiTaskes(),
  ];
  void changeIndex(int index) {
    currentIndex = index;
    emit(AppChangeBottonNavBarState());
  }

  Database database;

  void createDatabase() async {
    database = await openDatabase(
      'todo.bd',
      version: 1,
      onCreate: (database, version) {
        print('database create');
        database
            .execute(
                'CREATE TABLE tasks  (id INTEGER PRIMARY KEY, title TEXT, date TEXT, time TEXT, status TEXT)')
            .then((value) {
          print('database createed');
        }).catchError((error) {
          print('Error when create ${error.toString()}');
        });
      },
      onOpen: (database) {
        print('database open');
      },
    );
  }

  void insertToDatabase() {
    database.transaction((txn) {
      txn
          .rawInsert(
        'INSERT INTO tasks (title, date, time, status) VALUES("first taske", "2020", "12", "new")',
      )
          .then((value) {
        print('$value inserted ok');
      }).catchError((error) {
        print('Error when cinserting new ${error.toString()}');
      });
      return null;
    });
  }
}
