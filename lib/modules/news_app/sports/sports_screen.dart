import 'package:app/layout/news_app/cubit/cubit.dart';
import 'package:app/layout/news_app/cubit/states.dart';
import 'package:app/shared/componants/components.dart';
import 'package:conditional_builder/conditional_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SportsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<NewsCubit, NewsStates>(
        listener: (context, state) {},
        builder: (context, state) {
          var list = NewsCubit.get(context).sports;
          return ConditionalBuilder(
            condition: list.length > 0,
            // condition: state is! NewsGetSportsLoadingState,
            builder: (context) => ListView.separated(
              physics: BouncingScrollPhysics(),
              itemBuilder: (context, index) => ConditionalBuilder(
                condition: list.length > 0,
                // condition: state is! NewsGetBusinessLoadingState,
                builder: (context) => ListView.separated(
                  physics: BouncingScrollPhysics(),
                  itemBuilder: (context, index) =>
                      buildeArticleItem(list[index], context),
                  separatorBuilder: (context, index) => Container(),
                  itemCount: 10,
                ),
                fallback: (context) =>
                    Center(child: CircularProgressIndicator()),
              ),
              separatorBuilder: (context, index) => Container(),
              itemCount: 10,
            ),
            fallback: (context) => Center(child: CircularProgressIndicator()),
          );
        },
      ),
    );
  }
}
