import 'package:app/layout/news_app/cubit/cubit.dart';
import 'package:app/layout/news_app/cubit/states.dart';
import 'package:app/shared/componants/components.dart';
import 'package:app/shared/styles/healper/TextFormFields.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SearchScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var searchController = TextEditingController();
    return BlocConsumer<NewsCubit, NewsStates>(
      listener: (context, state) {},
      builder: (context, state) {
        var list = NewsCubit.get(context).search;
        return Scaffold(
          appBar: AppBar(
            title: Text('Search'),
          ),
          body: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: txtField(
                    context: context,
                    controller: searchController,
                    enabled: true,
                    hintText: "searsh",
                    textInputType: TextInputType.text,
                    validator: (String value) {
                      if (value.isEmpty)
                        return "search";
                      else
                        return null;
                    },
                    onSaved: (value) {
                      NewsCubit.get(context).getSearsh(value);
                    },
                    obscureText: false,
                    prefix: "assets/icons/mail.png"),
              ),
              Expanded(
                child: Expanded(
                  child: articleBuilder(
                    list,
                    context,
                    isSearch: true,
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
