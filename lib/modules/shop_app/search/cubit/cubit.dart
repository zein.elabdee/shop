import 'package:app/models/shop_app_model/shearch_model/shearch_model.dart';
import 'package:app/modules/shop_app/search/cubit/states.dart';
import 'package:app/shared/componants/components.dart';
import 'package:app/shared/netowrk/end_poinet/end_pointes.dart';
import 'package:app/shared/netowrk/remote/dio_helper/dio_helper.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SearchCubit extends Cubit<SearchStates> {
  SearchCubit() : super(SearchInitialState());
  static SearchCubit get(context) => BlocProvider.of(context);

  SearchModel searchModel;
  void search(String text) {
    emit(SearchLoadingState());
    DioHelper.postData(
      url: SEARCH,
      token: token,
      data: {
        'text': text,
      },
    ).then((value) {
      searchModel = SearchModel.fromJson(value.data);
      print(value.data);
      emit(SearchSuccessState());
    }).catchError((error) {
      print(error.toString());
      emit(SearchErrorState(error));
    });
  }
}
