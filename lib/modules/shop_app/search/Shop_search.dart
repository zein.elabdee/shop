import 'package:app/modules/shop_app/search/cubit/cubit.dart';
import 'package:app/modules/shop_app/search/cubit/states.dart';
import 'package:app/shared/styles/healper/TextFormFields.dart';
import 'package:app/shared/styles/healper/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app/shared/componants/components.dart';

class ShopSearch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var formKey = GlobalKey<FormState>();
    var searchController = TextEditingController();
    return BlocProvider(
      create: (BuildContext context) => SearchCubit(),
      child: BlocConsumer<SearchCubit, SearchStates>(
        listener: (context, state) {},
        builder: (context, state) {
          return Scaffold(
            appBar: AppBar(
              title: Text(
                'ShopSearch',
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
            ),
            body: Form(
              key: formKey,
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  children: [
                    txtFieldSearch(
                      context: context,
                      controller: searchController,
                      enabled: true,
                      hintText: "search",
                      textInputType: TextInputType.text,
                      supmmit: (String text) {
                        SearchCubit.get(context).search(text);
                      },
                      validator: (String val) {
                        if (val.isEmpty)
                          return "search";
                        else
                          return null;
                      },
                      // onSaved: (String text) {
                      //   SearchCubit.get(context).search(text);
                      // },
                      obscureText: false,
                      prefix: "assets/icons/mail.png",
                    ),
                    SizedBox(
                      height: AppTheme.sizedBoxHeight,
                    ),
                    if (state is SearchLoadingState) LinearProgressIndicator(),
                    SizedBox(
                      height: AppTheme.sizedBoxHeight,
                    ),
                    if (state is SearchSuccessState)
                      Expanded(
                        child: ListView.separated(
                            itemBuilder: (context, index) =>
                                builderListProdactes(
                                    SearchCubit.get(context)
                                        .searchModel
                                        .data
                                        .data[index],
                                    context,
                                    isOldPrise: false),
                            separatorBuilder: (context, index) => Divider(),
                            itemCount: SearchCubit.get(context)
                                .searchModel
                                .data
                                .data
                                .length),
                      ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
