import 'package:app/models/shop_app_model/auth_model/login_model/login_model.dart';
import 'package:app/modules/shop_app/auth/rgister_screen/cubit/states.dart';
import 'package:app/shared/netowrk/end_poinet/end_pointes.dart';
import 'package:app/shared/netowrk/remote/dio_helper/dio_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ShopRegisterCubit extends Cubit<ShopRegisterStates> {
  ShopRegisterCubit() : super(ShopRegisterInitialState());
  static ShopRegisterCubit get(context) => BlocProvider.of(context);
  ShopLoginModel shopLoginModel;
  void userRegister({
    @required String email,
    @required String password,
    @required String phone,
    @required String name,
  }) {
    emit(ShopRegisterLoadingState());
    DioHelper.postData(
      url: REGISTER,
      data: {
        'name': name,
        'phone': phone,
        'email': email,
        'password': password,
      },
    ).then((value) {
      print(value.data);
      shopLoginModel = ShopLoginModel.fromJson(value.data);
      emit(ShopRegisterSuccessState(shopLoginModel));
    }).catchError((error) {
      print(error.toString());
      emit(ShopRegisterErrorState(error.toString()));
    });
  }

  IconData suffex = Icons.visibility;
  bool isPassword = true;

  void chungPasVs() {
    isPassword = !isPassword;
    suffex = isPassword ? Icons.visibility : Icons.visibility_off;
    emit(ShopRegisterChungPassState());
  }
}
