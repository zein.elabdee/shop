import 'package:app/layout/shop_app/shop_layout/shop_layout.dart';
import 'package:app/modules/shop_app/auth/login_screen/cubit/cubit.dart';
import 'package:app/modules/shop_app/auth/login_screen/cubit/states.dart';
import 'package:app/modules/shop_app/auth/rgister_screen/cubit/states.dart';
import 'package:app/shared/componants/components.dart';
import 'package:app/shared/netowrk/local/cach_healper/cach_healper.dart';
import 'package:app/shared/styles/healper/Btns.dart';
import 'package:app/shared/styles/healper/TextFormFields.dart';
import 'package:app/shared/styles/healper/app_theme.dart';
import 'package:conditional_builder/conditional_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'cubit/cubit.dart';

class ShopRegisterScreen extends StatefulWidget {
  @override
  _ShopRegisterScreenState createState() => _ShopRegisterScreenState();
}

class _ShopRegisterScreenState extends State<ShopRegisterScreen> {
  @override
  Widget build(BuildContext context) {
    bool obscureText = true;
    bool isPassword = true;
    var formKey = GlobalKey<FormState>();

    var nameControler = TextEditingController();
    var phoneControler = TextEditingController();
    var emailControler = TextEditingController();
    var passowrdControler = TextEditingController();

    return BlocProvider(
      create: (BuildContext context) => ShopRegisterCubit(),
      child: BlocConsumer<ShopRegisterCubit, ShopRegisterStates>(
        listener: (context, state) {
          if (state is ShopRegisterSuccessState) {
            if (state.shopLoginModel.status) {
              print(state.shopLoginModel.data.token);
              print(state.shopLoginModel.message);
              showMassege(
                state: ToastStates.SUCCESS,
                text: state.shopLoginModel.message,
              );
              CachHealper.saveData(
                key: 'token',
                value: state.shopLoginModel.data.token,
              ).then((value) {
                token = state.shopLoginModel.data.token;
                navigateAndFinsh(
                  context,
                  ShopLayOutScreen(),
                );
              });
            } else {
              print(state.shopLoginModel.message);
              showMassege(
                state: ToastStates.ERROR,
                text: state.shopLoginModel.message,
              );
            }
          }
        },
        builder: (context, state) => Scaffold(
          body: Center(
            child: SingleChildScrollView(
              child: Form(
                key: formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                        right: 20,
                        left: 20,
                        top: 15,
                        bottom: 15,
                      ),
                      child: Text(
                        "SIGN UP",
                        style: TextStyle(
                          fontSize: 40,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: AppTheme.sizedBoxHeight,
                    ),
                    txtField(
                      context: context,
                      controller: nameControler,
                      enabled: true,
                      hintText: "name",
                      textInputType: TextInputType.text,
                      validator: (String val) {
                        if (val.isEmpty)
                          return "الخانه مطلوبه";
                        else
                          return null;
                      },
                      onSaved: (String val) {},
                      obscureText: false,
                      prefix: "assets/icons/mail.png",
                    ),
                    SizedBox(
                      height: AppTheme.sizedBoxHeight,
                    ),
                    txtField(
                      context: context,
                      controller: phoneControler,
                      enabled: true,
                      hintText: "phone",
                      textInputType: TextInputType.phone,
                      validator: (String val) {
                        if (val.isEmpty)
                          return "الخانه مطلوبه";
                        else
                          return null;
                      },
                      onSaved: (String val) {},
                      obscureText: false,
                      prefix: "assets/icons/mail.png",
                    ),
                    SizedBox(
                      height: AppTheme.sizedBoxHeight,
                    ),
                    txtField(
                      context: context,
                      controller: emailControler,
                      enabled: true,
                      hintText: "email",
                      textInputType: TextInputType.emailAddress,
                      validator: (String val) {
                        if (val.isEmpty)
                          return "الخانه مطلوبه";
                        else
                          return null;
                      },
                      onSaved: (String val) {},
                      obscureText: false,
                      prefix: "assets/icons/mail.png",
                    ),
                    SizedBox(
                      height: AppTheme.sizedBoxHeight,
                    ),
                    txtField(
                      context: context,
                      validator: (String val) {
                        if (val.isEmpty)
                          "كلمة المرور مطلوبة";
                        else
                          return null;
                      },
                      onSaved: (String val) {
                        setState(() {});
                      },
                      suffixx: ShopRegisterCubit.get(context).suffex,
                      isPasswerd: ShopRegisterCubit.get(context).isPassword,
                      suffixpassword: () {
                        ShopRegisterCubit.get(context).chungPasVs();
                      },
                      controller: passowrdControler,
                      textInputType: TextInputType.visiblePassword,
                      enabled: true,
                      prefix: "assets/icons/password.png",
                      obscureText: obscureText,
                      hintText: "كلمة المرور",
                    ),
                    SizedBox(
                      height: AppTheme.sizedBoxHeight,
                    ),
                    SizedBox(
                      height: AppTheme.sizedBoxHeight,
                    ),
                    ConditionalBuilder(
                      condition: state is! ShopRegisterLoadingState,
                      fallback: (context) =>
                          Center(child: CircularProgressIndicator()),
                      builder: (context) {
                        return btn(
                          txt: "Sign Up",
                          onTap: () {
                            if (formKey.currentState.validate()) {
                              ShopRegisterCubit.get(context).userRegister(
                                email: emailControler.text,
                                password: passowrdControler.text,
                                phone: passowrdControler.text,
                                name: passowrdControler.text,
                              );
                            }

                            // print("zein");
                          },
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
