import 'package:app/layout/shop_app/shop_layout/shop_layout.dart';
import 'package:app/modules/shop_app/auth/login_screen/cubit/cubit.dart';
import 'package:app/modules/shop_app/auth/rgister_screen/shop_register_screen.dart';
import 'package:app/shared/componants/components.dart';
import 'package:app/shared/netowrk/local/cach_healper/cach_healper.dart';
import 'package:app/shared/styles/healper/Btns.dart';
import 'package:app/shared/styles/healper/TextFormFields.dart';
import 'package:app/shared/styles/healper/app_theme.dart';
import 'package:conditional_builder/conditional_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'cubit/states.dart';

class ShopLoginScreen extends StatefulWidget {
  @override
  _ShopLoginScreenState createState() => _ShopLoginScreenState();
}

class _ShopLoginScreenState extends State<ShopLoginScreen> {
  bool obscureText = true;
  bool isPassword = true;
  var formKey = GlobalKey<FormState>();
  var emailControler = TextEditingController();
  var passowrdControler = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => ShopLoginCubit(),
      child: BlocConsumer<ShopLoginCubit, ShopLoginStates>(
        listener: (context, state) {
          if (state is ShopLoginSuccessState) {
            if (state.shopLoginModel.status) {
              print(state.shopLoginModel.data.token);
              print(state.shopLoginModel.message);
              showMassege(
                state: ToastStates.SUCCESS,
                text: state.shopLoginModel.message,
              );
              CachHealper.saveData(
                key: 'token',
                value: state.shopLoginModel.data.token,
              ).then((value) {
                token = state.shopLoginModel.data.token;
                navigateAndFinsh(
                  context,
                  ShopLayOutScreen(),
                );
              });
            } else {
              print(state.shopLoginModel.message);
              showMassege(
                state: ToastStates.ERROR,
                text: state.shopLoginModel.message,
              );
            }
          }
        },
        builder: (context, state) {
          return Scaffold(
            appBar: AppBar(),
            body: Center(
              child: SingleChildScrollView(
                child: Form(
                  key: formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                          right: 20,
                          left: 20,
                          top: 15,
                          bottom: 15,
                        ),
                        child: Text(
                          "SIGN IN",
                          style: TextStyle(
                            fontSize: 40,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: AppTheme.sizedBoxHeight,
                      ),
                      txtField(
                        context: context,
                        controller: emailControler,
                        enabled: true,
//                  hintText: translator.currentLanguage == "en"
//                      ? "Email/mobile"
//                      : "البريد الالكترونى/رقم الجوال",
//                  textInputType: TextInputType.text,
                        hintText: "email",
                        textInputType: TextInputType.emailAddress,
                        validator: (String val) {
                          if (val.isEmpty)
                            return "الخانه مطلوبه";
                          else
                            return null;
                        },
                        onSaved: (String val) {
                          setState(() {});
                        },
                        obscureText: false,
                        prefix: "assets/icons/mail.png",
                      ),
                      SizedBox(
                        height: AppTheme.sizedBoxHeight,
                      ),
                      txtField(
                        context: context,
                        validator: (String val) {
                          if (val.isEmpty)
                            "كلمة المرور مطلوبة";
                          else
                            return null;
                        },
                        onSaved: (String val) {
                          setState(() {});
                        },
                        suffixx: ShopLoginCubit.get(context).suffex,
                        isPasswerd: ShopLoginCubit.get(context).isPassword,
                        suffixpassword: () {
                          ShopLoginCubit.get(context).chungPasVs();
                        },
                        controller: passowrdControler,
                        textInputType: TextInputType.visiblePassword,
                        enabled: true,
                        prefix: "assets/icons/password.png",
                        obscureText: obscureText,
                        hintText: "كلمة المرور",
                      ),
                      SizedBox(
                        height: AppTheme.sizedBoxHeight,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text('ليس لديك حساب ؟'),
                          SizedBox(
                            width: AppTheme.sizedBoxHeight - 10,
                          ),
                          defaultTextButton(
                            text: 'سجل الان',
                            onpressd: () {
                              navigateTo(
                                context,
                                ShopRegisterScreen(),
                              );
                            },
                          ),
                        ],
                      ),
                      SizedBox(
                        height: AppTheme.sizedBoxHeight,
                      ),
                      ConditionalBuilder(
                        condition: state is! ShopLoginLoadingState,
                        fallback: (context) =>
                            Center(child: CircularProgressIndicator()),
                        builder: (context) {
                          return btn(
                            txt: "Login",
                            onTap: () {
                              if (formKey.currentState.validate()) {
                                ShopLoginCubit.get(context).userLogin(
                                  email: emailControler.text,
                                  password: passowrdControler.text,
                                );
                              }

                              // print("zein");
                            },
                          );
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
