import 'package:app/models/shop_app_model/auth_model/login_model/login_model.dart';
import 'package:app/modules/shop_app/auth/login_screen/cubit/states.dart';
import 'package:app/shared/netowrk/end_poinet/end_pointes.dart';
import 'package:app/shared/netowrk/remote/dio_helper/dio_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ShopLoginCubit extends Cubit<ShopLoginStates> {
  ShopLoginCubit() : super(ShopLoginInitialState());
  static ShopLoginCubit get(context) => BlocProvider.of(context);
  ShopLoginModel shopLoginModel;
  void userLogin({
    @required String email,
    @required String password,
  }) {
    emit(ShopLoginLoadingState());
    DioHelper.postData(
      url: LOGIN,
      data: {
        'email': email,
        'password': password,
      },
    ).then((value) {
      print(value.data);
      shopLoginModel = ShopLoginModel.fromJson(value.data);
      emit(ShopLoginSuccessState(shopLoginModel));
    }).catchError((error) {
      print(error.toString());
      emit(ShopLoginErrorState(error.toString()));
    });
  }

  IconData suffex = Icons.visibility;
  bool isPassword = true;

  void chungPasVs() {
    isPassword = !isPassword;
    suffex = isPassword ? Icons.visibility : Icons.visibility_off;
    emit(ShopChungPassState());
  }
}
