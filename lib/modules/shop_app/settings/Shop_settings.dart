import 'package:app/layout/shop_app/shop_layout/cubit/cubit.dart';
import 'package:app/layout/shop_app/shop_layout/cubit/states.dart';
import 'package:app/modules/shop_app/auth/login_screen/login_screen.dart';
import 'package:app/shared/componants/components.dart';
import 'package:app/shared/componants/observe/bloc_%20observe.dart';
import 'package:app/shared/netowrk/local/cach_healper/cach_healper.dart';
import 'package:app/shared/styles/healper/Btns.dart';
import 'package:app/shared/styles/healper/TextFormFields.dart';
import 'package:app/shared/styles/healper/app_theme.dart';
import 'package:conditional_builder/conditional_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ShopSettinges extends StatelessWidget {
  var formKey = GlobalKey<FormState>();
  var nameController = TextEditingController();
  var emailController = TextEditingController();
  var phoneController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ShopCubit, ShopStates>(
      listener: (context, state) {
        // if (state is ShopSuccessSettingsState) {
        //   nameController.text = state.loginModel.data.name;
        //   emailController.text = state.loginModel.data.email;
        //   phoneController.text = state.loginModel.data.phone;
        // }
      },
      builder: (context, state) {
        var model = ShopCubit.get(context).shopLoginModel;
        nameController.text = model.data.name;
        emailController.text = model.data.email;
        phoneController.text = model.data.phone;
        return ConditionalBuilder(
          condition: ShopCubit.get(context).shopLoginModel != null,
          builder: (context) {
            return Padding(
              padding: const EdgeInsets.all(20.0),
              child: Form(
                key: formKey,
                child: Column(
                  children: [
                    if (state is ShopLoadingUpdateProfileState)
                      LinearProgressIndicator(),
                    SizedBox(
                      height: AppTheme.sizedBoxHeight,
                    ),
                    // Image(
                    //   image: NetworkImage(model.data.image.toString()),
                    // ),
                    txtField(
                      context: context,
                      controller: nameController,
                      enabled: true,
                      hintText: "name",
                      textInputType: TextInputType.text,
                      validator: (String val) {
                        if (val.isEmpty)
                          return "الخانه مطلوبه";
                        else
                          return null;
                      },
                      onSaved: (String val) {},
                      obscureText: false,
                      prefix: "assets/icons/mail.png",
                    ),
                    SizedBox(
                      height: AppTheme.sizedBoxHeight,
                    ),
                    txtField(
                      context: context,
                      controller: emailController,
                      enabled: true,
                      hintText: "email",
                      textInputType: TextInputType.emailAddress,
                      validator: (String val) {
                        if (val.isEmpty)
                          return "الخانه مطلوبه";
                        else
                          return null;
                      },
                      onSaved: (String val) {},
                      obscureText: false,
                      prefix: "assets/icons/mail.png",
                    ),
                    SizedBox(
                      height: AppTheme.sizedBoxHeight,
                    ),
                    txtField(
                      context: context,
                      controller: phoneController,
                      enabled: true,
                      hintText: "phone",
                      textInputType: TextInputType.phone,
                      validator: (String val) {
                        if (val.isEmpty)
                          return "الخانه مطلوبه";
                        else
                          return null;
                      },
                      onSaved: (String val) {},
                      obscureText: false,
                      prefix: "assets/icons/mail.png",
                    ),
                    SizedBox(
                      height: AppTheme.sizedBoxHeight,
                    ),

                    btn(
                      txt: "Update",
                      onTap: () {
                        if (formKey.currentState.validate()) {
                          ShopCubit.get(context).updateUserData(
                            name: nameController.text,
                            email: emailController.text,
                            phone: phoneController.text,
                          );
                        }
                      },

                      // print("zein");
                    ),
                    SizedBox(
                      height: AppTheme.sizedBoxHeight,
                    ),

                    btn(
                      txt: "Out",
                      onTap: () {
                        CachHealper.removeData(
                          key: 'token',
                        ).then((value) {
                          navigateAndFinsh(context, ShopLoginScreen());
                        });
                      },

                      // print("zein");
                    ),
                  ],
                ),
              ),
            );
          },
          fallback: (context) => Center(child: CircularProgressIndicator()),
        );
      },
    );
  }
}
