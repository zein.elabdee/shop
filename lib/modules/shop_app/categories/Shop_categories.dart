import 'package:app/layout/shop_app/shop_layout/cubit/cubit.dart';
import 'package:app/layout/shop_app/shop_layout/cubit/states.dart';
import 'package:app/models/shop_app_model/categories_model/categories_model.dart';
import 'package:app/shared/styles/healper/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ShopCategories extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<ShopCubit, ShopStates>(
        listener: (context, state) {},
        builder: (BuildContext context, state) => ListView.separated(
          itemBuilder: (context, index) => builderCateItem(
              ShopCubit.get(context).categoriesModel.data.data[index]),
          separatorBuilder: (context, index) => Divider(),
          itemCount: ShopCubit.get(context).categoriesModel.data.data.length,
        ),
      ),
    );
  }

  Widget builderCateItem(Datum model) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Row(
        children: [
          Image(
            image: NetworkImage(
              model.image,
            ),
            height: 80.0,
            width: 80.0,
            fit: BoxFit.cover,
          ),
          SizedBox(
            width: AppTheme.sizedBoxHeight,
          ),
          Text(
            model.name,
            style: AppTheme.materiaCategoris,
          ),
          Spacer(),
          Icon(
            Icons.arrow_forward_ios,
          )
        ],
      ),
    );
  }
}
