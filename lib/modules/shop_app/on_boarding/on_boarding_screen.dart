import 'package:app/modules/shop_app/auth/login_screen/login_screen.dart';
import 'package:app/shared/componants/components.dart';
import 'package:app/shared/netowrk/local/cach_healper/cach_healper.dart';
import 'package:app/shared/styles/healper/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class BoardingModel {
  final String image;
  final String title;
  final String body;

  BoardingModel({this.image, this.title, this.body});
}

class OnBoradingScreen extends StatefulWidget {
  @override
  _OnBoradingScreenState createState() => _OnBoradingScreenState();
}

class _OnBoradingScreenState extends State<OnBoradingScreen> {
  final boardiControler = PageController();
  bool isList = false;
  void submit() {
    CachHealper.saveData(
      key: 'onBoarding',
      value: true,
    ).then((value) {
      if (value) {
        navigateAndFinsh(context, ShopLoginScreen());
      }
    });
  }

  List<BoardingModel> boarding = [
    BoardingModel(
      image: 'assets/icons/kisspng.png',
      title: 'Screen page 1',
      body: 'Screen body 1',
    ),
    BoardingModel(
      image: 'assets/icons/kisspng.png',
      title: 'Screen page 2',
      body: 'Screen body 2',
    ),
    BoardingModel(
      image: 'assets/icons/kisspng.png',
      title: 'Screen page 3',
      body: 'Screen body 3',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        actions: [
          TextButton(
            onPressed: submit,
            child: Text(
              'skep',
            ),
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(30.0),
        child: Column(
          children: [
            Expanded(
              child: PageView.builder(
                controller: boardiControler,
                onPageChanged: (int index) {
                  if (index == boarding.length - 1) {
                    setState(() {
                      isList = true;
                    });
                  } else {
                    setState(() {
                      isList = false;
                    });
                  }
                },
                itemCount: boarding.length,
                itemBuilder: (context, index) =>
                    buildBoardingItem(boarding[index]),
              ),
            ),
            SizedBox(
              height: AppTheme.sizedBoxHeight + 20,
            ),
            Row(
              children: [
                SmoothPageIndicator(
                  controller: boardiControler,
                  count: boarding.length,
                  effect: ExpandingDotsEffect(
                    dotColor: Colors.grey,
                    activeDotColor: Colors.orange,
                    dotHeight: 10,
                    dotWidth: 10,
                    expansionFactor: 4,
                    spacing: 5,
                  ),
                ),
                Spacer(),
                FloatingActionButton(
                  onPressed: () {
                    if (isList) {
                      submit();
                    } else {
                      boardiControler.nextPage(
                        duration: Duration(
                          milliseconds: 750,
                        ),
                        curve: Curves.fastLinearToSlowEaseIn,
                      );
                    }
                  },
                  child: Icon(
                    Icons.arrow_right_alt,
                    size: 22,
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget buildBoardingItem(BoardingModel model) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: Image.asset(
            '${model.image}',
            fit: BoxFit.contain,
          ),
        ),
        Text(
          '${model.title}',
          style: AppTheme.textTitleStyle,
        ),
        SizedBox(
          height: AppTheme.sizedBoxHeight - 10,
        ),
        Text(
          '${model.body}',
          style: TextStyle(
            fontSize: 14.0,
            color: Colors.grey,
          ),
        ),
      ],
    );
  }
}
