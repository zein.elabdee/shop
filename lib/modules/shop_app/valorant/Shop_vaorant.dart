import 'package:app/layout/shop_app/shop_layout/cubit/cubit.dart';
import 'package:app/layout/shop_app/shop_layout/cubit/states.dart';
import 'package:app/shared/componants/components.dart';
import 'package:conditional_builder/conditional_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ShopVavorant extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ShopCubit, ShopStates>(
      listener: (context, state) {},
      builder: (BuildContext context, state) => ConditionalBuilder(
        condition: state is! ShopLoadingGetFavoritesState,
        builder: (context) => ListView.separated(
          itemBuilder: (context, index) => builderListProdactes(
            ShopCubit.get(context).favoriteModel.data.data[index].product,
            context,
          ),
          separatorBuilder: (context, index) => Divider(),
          itemCount: ShopCubit.get(context).favoriteModel.data.data.length,
        ),
        fallback: (context) => Center(child: CircularProgressIndicator()),
      ),
    );
  }
}
