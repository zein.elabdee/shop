import 'package:app/shared/styles/healper/Btns.dart';
import 'package:app/shared/styles/healper/TextFormFields.dart';
import 'package:app/shared/styles/healper/app_theme.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool obscureText = true;
  bool isPassword = true;
  var formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: SingleChildScrollView(
          child: Form(
            key: formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                    right: 20,
                    left: 20,
                    top: 15,
                    bottom: 15,
                  ),
                  child: Text(
                    "SIGN IN",
                    style: TextStyle(
                      fontSize: 40,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                SizedBox(
                  height: AppTheme.sizedBoxHeight,
                ),
                txtField(
                    context: context,
                    controller: null,
                    enabled: true,
//                  hintText: translator.currentLanguage == "en"
//                      ? "Email/mobile"
//                      : "البريد الالكترونى/رقم الجوال",
//                  textInputType: TextInputType.text,
                    hintText: "email",
                    textInputType: TextInputType.emailAddress,
                    validator: (String val) {
                      if (val.isEmpty)
                        return "email";
                      else
                        return null;
                    },
                    onSaved: (String val) {
                      setState(() {});
                    },
                    obscureText: false,
                    prefix: "assets/icons/mail.png"),
                SizedBox(
                  height: AppTheme.sizedBoxHeight,
                ),
                txtField(
                  context: context,
                  validator: (String val) {
                    if (val.isEmpty)
                      "كلمة المرور مطلوبة";
                    else
                      return null;
                  },
                  onSaved: (String val) {
                    setState(() {});
                  },
                  suffixx: isPassword ? Icons.visibility : Icons.visibility_off,
                  isPasswerd: isPassword,
                  suffixpassword: () {
                    setState(() {
                      isPassword = !isPassword;
                    });
                  },
                  controller: null,
                  textInputType: TextInputType.visiblePassword,
                  enabled: true,
                  prefix: "assets/icons/password.png",
                  obscureText: obscureText,
                  hintText: "كلمة المرور",
                ),
                btn(
                  txt: "Login",
                  onTap: () {
                    // print("zein");
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
