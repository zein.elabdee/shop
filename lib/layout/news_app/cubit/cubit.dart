import 'package:app/layout/news_app/cubit/states.dart';
import 'package:app/modules/news_app/business/business_screen.dart';
import 'package:app/modules/news_app/science/science_screen.dart';
import 'package:app/modules/news_app/sports/sports_screen.dart';
import 'package:app/shared/netowrk/remote/dio_helper/dio_helper.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';

class NewsCubit extends Cubit<NewsStates> {
  NewsCubit() : super(NewsInitialState());
  static NewsCubit get(context) => BlocProvider.of(context);
  int currentIndex = 0;
  List<BottomNavigationBarItem> buttonItem = [
    BottomNavigationBarItem(
      icon: Icon(
        Icons.business,
      ),
      label: 'Business',
    ),
    BottomNavigationBarItem(
      icon: Icon(
        Icons.sports,
      ),
      label: 'Sports',
    ),
    BottomNavigationBarItem(
      icon: Icon(
        Icons.science,
      ),
      label: 'Science',
    ),
    // BottomNavigationBarItem(
    //   icon: Icon(
    //     Icons.settings,
    //   ),
    //   label: 'Settings',
    // ),
  ];

  List<Widget> screens = [
    BusinessScreen(),
    SportsScreen(),
    ScienceScreen(),
    // SettingsScreens(),
  ];
  void changeBottomNavigationBar(int index) {
    currentIndex = index;
    if (index == 1) getports();
    if (index == 2) getScience();
    emit(NewsChangeBottonNavBarState());
  }

  List<dynamic> business = [];
  void getBusiness() {
    emit(NewsGetBusinessLoadingState());
    DioHelper.getData(
      url: 'v2/top-headlines',
      query: {
        'country': 'eg',
        'apiKey': '2d5340780783460488a7b3cc851d1cff',
      },
    ).then((value) {
      // print(value.data['articles'][0]['description']);
      business = value.data['articles'];
      print(business[0]['title']);
      emit(NewsGetBusinessSuccessState());
    }).catchError((error) {
      print(error.toString());
      emit(NewsGetBusinessErrorState(error.toString()));
    });
  }

  // api sports
  List<dynamic> sports = [];
  void getports() {
    emit(NewsGetBusinessLoadingState());
    if (sports.length == 0) {
      DioHelper.getData(
        url: 'v2/top-headlines',
        query: {
          'country': 'eg',
          'category': 'sports',
          'apiKey': '2d5340780783460488a7b3cc851d1cff',
        },
      ).then((value) {
        // print(value.data['articles'][0]['description']);
        sports = value.data['articles'];
        print(sports[0]['title']);
        emit(NewsGetSportsSuccessState());
      }).catchError((error) {
        print(error.toString());
        emit(NewsGetSportsErrorState(error.toString()));
      });
    } else {
      emit(NewsGetSportsSuccessState());
    }
  }
  //api science

  List<dynamic> science = [];
  void getScience() {
    emit(NewsGetSciencsLoadingState());
    if (science.length == 0) {
      DioHelper.getData(
        url: 'v2/top-headlines',
        query: {
          'country': 'eg',
          'category': 'science',
          'apiKey': '2d5340780783460488a7b3cc851d1cff',
        },
      ).then((value) {
        // print(value.data['articles'][0]['description']);
        science = value.data['articles'];
        print(science[0]['title']);
        emit(NewsGetSciencsSuccessState());
      }).catchError((error) {
        print(error.toString());
        emit(NewsGetSciencsErrorState(error.toString()));
      });
    } else {
      emit(NewsGetSciencsSuccessState());
    }
  }

  // searsh app

  List<dynamic> search = [];
  void getSearsh(String value) {
    emit(NewsGetSearchLoadingState());
    DioHelper.getData(
      url: 'v2/everything',
      query: {
        'q': '$value',
        'apiKey': '2d5340780783460488a7b3cc851d1cff',
      },
    ).then((value) {
      // print(value.data['articles'][0]['description']);
      search = value.data['articles'];
      print(search[0]['title']);
      emit(NewsGetSciencsSuccessState());
    }).catchError((error) {
      print(error.toString());
      emit(NewsGetSearchErrorState(error.toString()));
    });
  }
}
