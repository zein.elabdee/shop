abstract class NewsStates {}

class NewsInitialState extends NewsStates {}

class NewsChangeBottonNavBarState extends NewsStates {}

class NewsGetBusinessLoadingState extends NewsStates {}

class NewsGetBusinessSuccessState extends NewsStates {}

class NewsGetBusinessErrorState extends NewsStates {
  final String error;

  NewsGetBusinessErrorState(this.error);
}

class NewsGetSportsLoadingState extends NewsStates {}

class NewsGetSportsSuccessState extends NewsStates {}

class NewsGetSportsErrorState extends NewsStates {
  final String error;

  NewsGetSportsErrorState(this.error);
}

class NewsGetSciencsLoadingState extends NewsStates {}

class NewsGetSciencsSuccessState extends NewsStates {}

class NewsGetSciencsErrorState extends NewsStates {
  final String error;

  NewsGetSciencsErrorState(this.error);
}

//search

class NewsGetSearchLoadingState extends NewsStates {}

class NewsGetSearchSuccessState extends NewsStates {}

class NewsGetSearchErrorState extends NewsStates {
  final String error;

  NewsGetSearchErrorState(this.error);
}
