import 'package:app/shared/cubit/cubit.dart';
import 'package:app/shared/cubit/states.dart';
import 'package:conditional_builder/conditional_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// class HomeLayout extends StatefulWidget {
//   @override
//   _HomeLayoutState createState() => _HomeLayoutState();
// }

class HomeLayout extends StatelessWidget {
  // @override
  // void initState() {
  //   createDatabase();
  //   super.initState();
  // }

  var scaffoldKey = GlobalKey<ScaffoldState>();
  bool isButtonShow = false;
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => AppCubit(),
      child: BlocConsumer<AppCubit, AppStates>(
        listener: (BuildContext context, AppStates state) {},
        builder: (BuildContext context, AppStates state) {
          AppCubit cubit = AppCubit.get(context);
          return Scaffold(
            key: scaffoldKey,
            appBar: AppBar(
              title: Text('HomeLayout'),
            ),
            body: ConditionalBuilder(
              condition: true,
              builder: (context) => cubit.screen[cubit.currentIndex],
              fallback: (context) => Center(child: CircularProgressIndicator()),
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: () {
                if (isButtonShow) {
                  Navigator.pop(context);
                  isButtonShow = false;
                } else {
                  scaffoldKey.currentState.showBottomSheet(
                    (context) => Container(
                      width: double.infinity,
                      height: 120,
                      color: Colors.red,
                    ),
                  );
                  isButtonShow = true;
                }
              },
              child: Icon(Icons.add),
            ),
            bottomNavigationBar: BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              currentIndex: cubit.currentIndex,
              onTap: (index) {
                cubit.changeIndex(index);
                // setState(() {
                //   currentIndex = index;
                // });
              },
              items: [
                BottomNavigationBarItem(
                  icon: Icon(
                    Icons.ac_unit,
                  ),
                  label: 'Home',
                ),
                BottomNavigationBarItem(
                  icon: Icon(
                    Icons.ac_unit,
                  ),
                  label: 'Home',
                ),
                BottomNavigationBarItem(
                  icon: Icon(
                    Icons.ac_unit,
                  ),
                  label: 'Home',
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
