import 'package:app/layout/shop_app/shop_layout/cubit/states.dart';
import 'package:app/models/shop_app_model/auth_model/login_model/login_model.dart';
import 'package:app/models/shop_app_model/categories_model/categories_model.dart';
import 'package:app/models/shop_app_model/change_favoritea_model/change_favoritea_model.dart';
import 'package:app/models/shop_app_model/favorites_model/favoritea_model.dart';
import 'package:app/models/shop_app_model/shop_home_model/shop_home_model.dart';
import 'package:app/modules/shop_app/categories/Shop_categories.dart';
import 'package:app/modules/shop_app/products/Shop_products.dart';
import 'package:app/modules/shop_app/settings/Shop_settings.dart';
import 'package:app/modules/shop_app/valorant/Shop_vaorant.dart';
import 'package:app/shared/componants/components.dart';
import 'package:app/shared/netowrk/end_poinet/end_pointes.dart';
import 'package:app/shared/netowrk/remote/dio_helper/dio_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ShopCubit extends Cubit<ShopStates> {
  ShopCubit() : super(ShopInitialState());
  static ShopCubit get(context) => BlocProvider.of(context);

  int currentIndex = 0;
  List<BottomNavigationBarItem> buttonItem = [
    BottomNavigationBarItem(
      icon: Icon(
        Icons.home,
      ),
      label: 'Home',
    ),
    BottomNavigationBarItem(
      icon: Icon(
        Icons.category,
      ),
      label: 'Category',
    ),
    BottomNavigationBarItem(
      icon: Icon(
        Icons.favorite,
      ),
      label: 'Favorite',
    ),
    BottomNavigationBarItem(
      icon: Icon(
        Icons.settings,
      ),
      label: 'Settings',
    ),
  ];
  List<Widget> screens = [
    ShopProducts(),
    ShopCategories(),
    ShopVavorant(),
    ShopSettinges(),
  ];
  void changeBottomNavigationBar(int index) {
    currentIndex = index;
    // if (index == 1) getports();
    // if (index == 2) getScience();
    emit(ShopChangeBottonNavBarState());
  }

  ShopHomeModel homeModel;
  Map<int, bool> favorites = {};
  void getShopHomeData() {
    emit(ShopLoadingHomeDataState());
    DioHelper.getData(
      url: HOME,
      token: token,
      // query: null,
    ).then((value) {
      homeModel = ShopHomeModel.fromJson(value.data);
      homeModel.data.products.forEach((element) {
        favorites.addAll({
          element.id: element.inFavorites,
        });
      });
      print(favorites.toString());
      // printFullText(homeModel.data.banners[0].image);
      emit(ShopSuccessHomeDataState());
    }).catchError((error) {
      print(error.toString());
      emit(ShopErrorHomeDataState(error));
    });
  }

  CategoriesModel categoriesModel;
  void getShopCategories() {
    // emit(ShopLoadingHomeDataState());
    DioHelper.getData(
      url: GET_CATEGORIES,
      token: token,
      // query: null,
    ).then((value) {
      categoriesModel = CategoriesModel.fromJson(value.data);
      // printFullText(categoriesModel.data.data[0].image);
      emit(ShopSuccessCategoriesState());
    }).catchError((error) {
      print(error.toString());
      emit(ShopErrorCategoriesState(error));
    });
  }

  // FAVORITES
  ChangeFavoriteaModel changeFavoriteaModel;
  void changeFavorites(int productId) {
    favorites[productId] = !favorites[productId];
    emit(ShopChangeFavoritesState());
    DioHelper.postData(
      url: FAVORITES,
      data: {
        'product_id': productId,
      },
      token: token,
    ).then((value) {
      changeFavoriteaModel = ChangeFavoriteaModel.formJson(value.data);
      if (!changeFavoriteaModel.status) {
        favorites[productId] = !favorites[productId];
      } else {
        getFavorites();
      }
      print(value.data);
      emit(ShopSuccessChangeFavoritesState(changeFavoriteaModel));
    }).catchError((erroe) {
      favorites[productId] = !favorites[productId];
      emit(ShopErrorChangeFavoritesState(erroe));
    });
  }

// Get FAVORITES

  FavoriteModel favoriteModel;
  void getFavorites() {
    emit(ShopLoadingGetFavoritesState());
    DioHelper.getData(
      url: FAVORITES,
      token: token,
      // query: null,
    ).then((value) {
      favoriteModel = FavoriteModel.fromJson(value.data);
      // printFullText(categoriesModel.data.data[0].image);
      emit(ShopSuccessGetFavoritesState());
    }).catchError((error) {
      print(error.toString());
      emit(ShopErrorGetFavoritesState(error));
    });
  }

  // update

  void updateUserData({
    @required String name,
    @required String email,
    @required String phone,
  }) {
    emit(ShopLoadingUpdateProfileState());
    DioHelper.putData(
      url: UPDATE_PROFILE,
      token: token,
      data: {
        'name': name,
        'email': email,
        'phone': phone,
      },
      // query: null,
    ).then((value) {
      shopLoginModel = ShopLoginModel.fromJson(value.data);
      // printFullText(categoriesModel.data.data[0].image);
      emit(ShopSuccessUpdateProfileState(shopLoginModel));
    }).catchError((error) {
      print(error.toString());
      emit(ShopErrorUpdateProfileState(error));
    });
  }

  // settings
  ShopLoginModel shopLoginModel;
  void getsettings() {
    emit(ShopLoadingGetFavoritesState());
    DioHelper.getData(
      url: PROFILE,
      token: token,
      // query: null,
    ).then((value) {
      shopLoginModel = ShopLoginModel.fromJson(value.data);
      printFullText(shopLoginModel.data.name);
      emit(ShopSuccessSettingsState(shopLoginModel));
    }).catchError((error) {
      print(error.toString());
      emit(ShopErrorSettingsState(error));
    });
  }
}
