import 'package:app/models/shop_app_model/auth_model/login_model/login_model.dart';
import 'package:app/models/shop_app_model/change_favoritea_model/change_favoritea_model.dart';

abstract class ShopStates {}

class ShopInitialState extends ShopStates {}

class ShopChangeBottonNavBarState extends ShopStates {}

class ShopLoadingHomeDataState extends ShopStates {}

class ShopSuccessHomeDataState extends ShopStates {}

class ShopErrorHomeDataState extends ShopStates {
  final String error;

  ShopErrorHomeDataState(this.error);
}

class ShopSuccessCategoriesState extends ShopStates {}

class ShopErrorCategoriesState extends ShopStates {
  final String error;

  ShopErrorCategoriesState(this.error);
}

// favorit

class ShopChangeFavoritesState extends ShopStates {}

class ShopSuccessChangeFavoritesState extends ShopStates {
  final ChangeFavoriteaModel model;

  ShopSuccessChangeFavoritesState(this.model);
}

class ShopErrorChangeFavoritesState extends ShopStates {
  final String error;

  ShopErrorChangeFavoritesState(this.error);
}

// get fav
class ShopLoadingGetFavoritesState extends ShopStates {}

class ShopSuccessGetFavoritesState extends ShopStates {}

class ShopErrorGetFavoritesState extends ShopStates {
  final String error;

  ShopErrorGetFavoritesState(this.error);
}

// settings
class ShopLoadingSettingsState extends ShopStates {}

class ShopSuccessSettingsState extends ShopStates {
  final ShopLoginModel loginModel;

  ShopSuccessSettingsState(this.loginModel);
}

class ShopErrorSettingsState extends ShopStates {
  final String error;

  ShopErrorSettingsState(this.error);
}

// update-profile
class ShopLoadingUpdateProfileState extends ShopStates {}

class ShopSuccessUpdateProfileState extends ShopStates {
  final ShopLoginModel loginModel;

  ShopSuccessUpdateProfileState(this.loginModel);
}

class ShopErrorUpdateProfileState extends ShopStates {
  final String error;

  ShopErrorUpdateProfileState(this.error);
}
