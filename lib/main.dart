import 'package:app/layout/shop_app/shop_layout/shop_layout.dart';
import 'package:app/modules/shop_app/auth/login_screen/login_screen.dart';
import 'package:app/shared/componants/components.dart';
import 'package:app/shared/cubit/cubit.dart';
import 'package:app/shared/cubit/states.dart';
import 'package:app/shared/netowrk/local/cach_healper/cach_healper.dart';
import 'package:app/shared/netowrk/remote/dio_helper/dio_helper.dart';
import 'package:app/shared/styles/healper/app_theme.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'layout/news_app/cubit/cubit.dart';
import 'layout/shop_app/shop_layout/cubit/cubit.dart';
import 'modules/shop_app/on_boarding/on_boarding_screen.dart';
import 'shared/componants/observe/bloc_ observe.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Bloc.observer = MyBlocObserver();
  DioHelper.init();
  await CachHealper.init();

  Widget widget;

  bool onBoarding = CachHealper.getData(key: 'onBoarding');
  token = CachHealper.getData(key: 'token');

  if (onBoarding != null) {
    if (token != null)
      widget = ShopLayOutScreen();
    else
      widget = ShopLoginScreen();
  } else {
    widget = OnBoradingScreen();
  }

  runApp(MyApp(
    startWidget: widget,
  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final Widget startWidget;

  MyApp({this.startWidget});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => NewsCubit()..getBusiness(),
        ),
        BlocProvider(
          create: (BuildContext context) => AppCubit(),
        ),
        BlocProvider(
          create: (BuildContext context) => ShopCubit()
            ..getShopHomeData()
            ..getShopCategories()
            ..getFavorites()
            ..getsettings(),
        ),
      ],
      child: BlocConsumer<AppCubit, AppStates>(
        listener: (context, state) {},
        builder: (context, state) {
          return MaterialApp(
            title: 'Flutter Demo',
            debugShowCheckedModeBanner: false,
            themeMode: ThemeMode.light,
            darkTheme: ThemeData(
                // scaffoldBackgroundColor: HexColor('333739'),
                ),
            theme: AppTheme.themeData,
            home: startWidget,
            // home: onBoarding ? ShopLoginScreen() : OnBoradingScreen(),
          );
        },
      ),
    );
  }
}
