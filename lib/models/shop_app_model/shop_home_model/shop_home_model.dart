// To parse this JSON data, do
//
//     final shopHomeModel = shopHomeModelFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

ShopHomeModel shopHomeModelFromJson(String str) =>
    ShopHomeModel.fromJson(json.decode(str));

String shopHomeModelToJson(ShopHomeModel data) => json.encode(data.toJson());

class ShopHomeModel {
  ShopHomeModel({
    @required this.status,
    @required this.message,
    @required this.data,
  });

  final bool status;
  final dynamic message;
  final Data data;

  ShopHomeModel copyWith({
    bool status,
    dynamic message,
    Data data,
  }) =>
      ShopHomeModel(
        status: status ?? this.status,
        message: message ?? this.message,
        data: data ?? this.data,
      );

  factory ShopHomeModel.fromJson(Map<String, dynamic> json) => ShopHomeModel(
        status: json["status"] == null ? null : json["status"],
        message: json["message"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
        "message": message,
        "data": data == null ? null : data.toJson(),
      };
}

class Data {
  Data({
    @required this.banners,
    @required this.products,
    @required this.ad,
  });

  final List<Banner> banners;
  final List<Product> products;
  final String ad;

  Data copyWith({
    List<Banner> banners,
    List<Product> products,
    String ad,
  }) =>
      Data(
        banners: banners ?? this.banners,
        products: products ?? this.products,
        ad: ad ?? this.ad,
      );

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        banners: json["banners"] == null
            ? null
            : List<Banner>.from(json["banners"].map((x) => Banner.fromJson(x))),
        products: json["products"] == null
            ? null
            : List<Product>.from(
                json["products"].map((x) => Product.fromJson(x))),
        ad: json["ad"] == null ? null : json["ad"],
      );

  Map<String, dynamic> toJson() => {
        "banners": banners == null
            ? null
            : List<dynamic>.from(banners.map((x) => x.toJson())),
        "products": products == null
            ? null
            : List<dynamic>.from(products.map((x) => x.toJson())),
        "ad": ad == null ? null : ad,
      };
}

class Banner {
  Banner({
    @required this.id,
    @required this.image,
    @required this.category,
    @required this.product,
  });

  final int id;
  final String image;
  final Category category;
  final dynamic product;

  Banner copyWith({
    int id,
    String image,
    Category category,
    dynamic product,
  }) =>
      Banner(
        id: id ?? this.id,
        image: image ?? this.image,
        category: category ?? this.category,
        product: product ?? this.product,
      );

  factory Banner.fromJson(Map<String, dynamic> json) => Banner(
        id: json["id"] == null ? null : json["id"],
        image: json["image"] == null ? null : json["image"],
        category: json["category"] == null
            ? null
            : Category.fromJson(json["category"]),
        product: json["product"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "image": image == null ? null : image,
        "category": category == null ? null : category.toJson(),
        "product": product,
      };
}

class Category {
  Category({
    @required this.id,
    @required this.image,
    @required this.name,
  });

  final int id;
  final String image;
  final String name;

  Category copyWith({
    int id,
    String image,
    String name,
  }) =>
      Category(
        id: id ?? this.id,
        image: image ?? this.image,
        name: name ?? this.name,
      );

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        id: json["id"] == null ? null : json["id"],
        image: json["image"] == null ? null : json["image"],
        name: json["name"] == null ? null : json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "image": image == null ? null : image,
        "name": name == null ? null : name,
      };
}

class Product {
  Product({
    @required this.id,
    @required this.price,
    @required this.oldPrice,
    @required this.discount,
    @required this.image,
    @required this.name,
    @required this.description,
    @required this.images,
    @required this.inFavorites,
    @required this.inCart,
  });

  final int id;
  final double price;
  final double oldPrice;
  final int discount;
  final String image;
  final String name;
  final String description;
  final List<String> images;
  final bool inFavorites;
  final bool inCart;

  Product copyWith({
    int id,
    double price,
    double oldPrice,
    int discount,
    String image,
    String name,
    String description,
    List<String> images,
    bool inFavorites,
    bool inCart,
  }) =>
      Product(
        id: id ?? this.id,
        price: price ?? this.price,
        oldPrice: oldPrice ?? this.oldPrice,
        discount: discount ?? this.discount,
        image: image ?? this.image,
        name: name ?? this.name,
        description: description ?? this.description,
        images: images ?? this.images,
        inFavorites: inFavorites ?? this.inFavorites,
        inCart: inCart ?? this.inCart,
      );

  factory Product.fromJson(Map<String, dynamic> json) => Product(
        id: json["id"] == null ? null : json["id"],
        price: json["price"] == null ? null : json["price"].toDouble(),
        oldPrice:
            json["old_price"] == null ? null : json["old_price"].toDouble(),
        discount: json["discount"] == null ? null : json["discount"],
        image: json["image"] == null ? null : json["image"],
        name: json["name"] == null ? null : json["name"],
        description: json["description"] == null ? null : json["description"],
        images: json["images"] == null
            ? null
            : List<String>.from(json["images"].map((x) => x)),
        inFavorites: json["in_favorites"] == null ? null : json["in_favorites"],
        inCart: json["in_cart"] == null ? null : json["in_cart"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "price": price == null ? null : price,
        "old_price": oldPrice == null ? null : oldPrice,
        "discount": discount == null ? null : discount,
        "image": image == null ? null : image,
        "name": name == null ? null : name,
        "description": description == null ? null : description,
        "images":
            images == null ? null : List<dynamic>.from(images.map((x) => x)),
        "in_favorites": inFavorites == null ? null : inFavorites,
        "in_cart": inCart == null ? null : inCart,
      };
}
